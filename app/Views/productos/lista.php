<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?=$title?></title>
    </head>
    <body>
         <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('')?>" method="post">
        <table class="table table-striped">
            <?php foreach ($familias as $familia): ?>
                <tr>
                    <td>
                        <h4><center>Familia</h4>
                        <?= $familia->NombreFamilia ?>
                    </td>
                    <td>
                        <h4><center>CodigoProducto</h4>
                        <?= $familia->CodigoProducto ?>
                    </td>
                    <td>
                        <h4><center>Nombre</h1>
                        <?= $familia->Nombre ?>
                    </td>
                    <td>
                        <h4><center>CodigoFamilia</h4>
                        <?= $familia->CodigoFamilia ?>
                    </td>
                    <td>
                        <h4><center>Caracteristicas</h4>
                        <?= $familia->Caracteristicas ?>
                    </td>
                    <td>
                        <h4><center>Color</h4>
                        <?= $familia->Color ?>
                    </td>
                    <td>
                        <h4><center>TipoIVA</h4>
                        <?= $familia->TipoIVA ?>
                    </td>
                    
                    <td>
                        <h4><center>Borrar</h4>
                        <a class="btn btn-primary" href="borrar.php?id=<?php echo $familia->CodigoProducto;?>"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></a>
                    </td>
                </tr>

            <?php endforeach; ?>
        </table>
        </form>
    </body>
</html>