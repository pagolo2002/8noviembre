<?php

?>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
       
        <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('InsertProd')?>" method="Post">
            <label for="texto" class="text-success">Introduce datos para añadir: </label><br>
            <input type="text" name="CodigoProducto" placeholder="CodigoProducto" id="CodigoProducto" /><br>
            <input type="text" name="Nombre" placeholder="Nombre"texto" id="Nombre"/><br>
            <input type="text" name="CodigoFamilia" placeholder="CodigoFamilia" id="CodigoFamilia" /><br>
            <input type="text" name="Caracteristicas" placeholder="Caracteristicas" id="Caracteristicas" /><br>
            <input type="text" name="Color" placeholder="Color" id="Color" /><br>
            <input type="text" name="TipoIVA" placeholder="TipoIVA" id="TipoIVA" /><br>
            <input type="submit" name="enviar" value="Enviar" />
        </form>
    </body>
</html>