<?php

namespace App\Controllers;
use App\Models\ProductosModel;


class Productos extends BaseController
{
    public function muestraProductos()
    {
        $data['title'] = 'Listado Productos';
        $productosModel = new \App\Models\ProductosModel();

        
        $data ['familias' ] = $productosModel
	->select ('*')
	->join('familias as fa','fa.CodigoFamilia=productos.CodigoFamilia','left') 
	->findAll();
        
        return view('productos/lista',$data);
    }
    public function InsertProd() {
       $InsertModel = new InsertModel(); 
        
       $CodigoProducto= $this->request->getPost("CodigoProducto");
       $Nombre= $this->request->getPost("Nombre");
       $CodigoFamilia= $this->request->getPost("CodigoFamilia");
       $Caracteristicas= $this->request->getPost("Caracteristicas");
       $Color= $this->request->getPost("Color");
       $TipoIVA= $this->request->getPost("TipoIVA");
       
       $producto = [
            "CodigoProducto"=>$CodigoProducto,
            "Nombre"=>$Nombre,
            "CodigoFamilia"=>$CodigoFamilia,
            "Caracteristicas"=>$Caracteristicas,
            "Color"=>$Color,
            "TipoIVA"=>$TipoIVA];
        
       $InsertModel->insert($producto);
       
       return redirect()->to('productos/lista');
    }
}
