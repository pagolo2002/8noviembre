<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

class InsertModel extends Model {
    
    
    protected $allowedFields = ['CodigoProducto','Nombre','CodigoFamilia','Caracteristicas','Color','TipoIVA'];
    protected $table = 'productos';
    protected $primaryKey = 'CodigoProducto';
    protected $returnType = 'object';
}
